#include "../src/np_common.h"

#include <check.h>
#include <stdlib.h>

START_TEST(ms2ts1)
{
    int ms = 1500;
    struct timespec ts = ms2timespec(ms);
    ck_assert_int_eq(1, ts.tv_sec);
    ck_assert_int_eq(500000000, ts.tv_nsec);
}
END_TEST

START_TEST(ms2ts2)
{
    int ms = 15000;
    struct timespec ts = ms2timespec(ms);
    ck_assert_int_eq(15, ts.tv_sec);
    ck_assert_int_eq(0, ts.tv_nsec);
}
END_TEST

START_TEST(read_hdr)
{
	unsigned char buf[4];
	buf[0] = 0;
	buf[1] = 0xaa;
	buf[2] = 0;
	buf[3] = 0xbb;

	np_hdr hdr;
	int num_read = read_np_hdr(buf, 4, &hdr);
	ck_assert_int_eq(4, num_read);
	ck_assert_int_eq(170, hdr.type);
	ck_assert_int_eq(187, hdr.seq);
}
END_TEST

START_TEST(write_hdr)
{
	unsigned char buf[4];

	np_hdr hdr;
	hdr.type = 170;
	hdr.seq = 187;
	int num_written = write_np_hdr(buf, 4, &hdr);
	ck_assert_int_eq(4, num_written);
	ck_assert_uint_eq(0, buf[0]);
	ck_assert_uint_eq(0xaa, buf[1]);
	ck_assert_uint_eq(0, buf[2]);
	ck_assert_uint_eq(0xbb, buf[3]);
}
END_TEST

START_TEST(rw_hdr)
{
	unsigned char buf[4];

	np_hdr hdr1, hdr2;
	hdr1.type = 170;
	hdr1.seq = 187;

	int num_written = write_np_hdr(buf, 4, &hdr1);
	ck_assert_int_eq(4, num_written);

	int num_read = read_np_hdr(buf, 4, &hdr2);
	ck_assert_int_eq(4, num_read);

	ck_assert_int_eq(hdr1.type, hdr2.type);
	ck_assert_int_eq(hdr1.seq, hdr2.seq);
}
END_TEST

START_TEST(rw_svr_time)
{
	unsigned char buf[8];

	np_svr_time t1, t2;
	t1.time = 0x1122334455667788;

	int num_written = write_np_svr_time(buf, 8, &t1);
	ck_assert_int_eq(8, num_written);
	ck_assert_uint_eq(0x11, buf[0]);
	ck_assert_uint_eq(0x22, buf[1]);
	ck_assert_uint_eq(0x33, buf[2]);
	ck_assert_uint_eq(0x44, buf[3]);
	ck_assert_uint_eq(0x55, buf[4]);
	ck_assert_uint_eq(0x66, buf[5]);
	ck_assert_uint_eq(0x77, buf[6]);
	ck_assert_uint_eq(0x88, buf[7]);

	int num_read = read_np_svr_time(buf, 8, &t2);
	ck_assert_int_eq(8, num_read);

	ck_assert_uint_eq(t1.time, t2.time);
}
END_TEST

Suite *np_suite(void) {
	Suite *s;
	TCase *tc_common;

	s = suite_create("np");
	tc_common = tcase_create("common");

	tcase_add_test(tc_common, ms2ts1);
	tcase_add_test(tc_common, ms2ts2);
	tcase_add_test(tc_common, read_hdr);
	tcase_add_test(tc_common, write_hdr);
	tcase_add_test(tc_common, rw_hdr);
	tcase_add_test(tc_common, rw_svr_time);

	suite_add_tcase(s, tc_common);

	return s;
}

int main(void)
{
	Suite *s = np_suite();
	SRunner *sr = srunner_create(s);
    srunner_set_log(sr, "test.log");
    srunner_set_xml(sr, "test.xml");
    //srunner_set_xml_format(CK_XML_FORMAT_JUNIT);

	srunner_run_all(sr, CK_NORMAL);
	int num_failed = srunner_ntests_failed(sr);

	srunner_free(sr);

	return (num_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
