#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <unistd.h>

#include "np_common.h"

void usage()
{
    printf("npc <host>\n"
           "  -h, --help         print help\n"
           "  -i, --interval     ping interval in ms\n");
}

int main(int argc, char *argv[])
{
    const int buffer_size = 1000;
	unsigned char buffer[buffer_size];

    //  Configure program options
    const int default_ping_interval_ms = 1000;
    int ping_interval_ms = default_ping_interval_ms;
    struct option long_opts[] =
    {
        { "help",               no_argument,        0, 'h' },
        { "interval",           required_argument,  0, 'i' },
        { 0, 0, 0, 0 }
    };

    const char *optstr = "i:";
    int opt;
    while((opt = getopt_long(argc, argv, optstr, long_opts, &optind)) != -1)
    {
        switch(opt)
        {
        case 'h':
            usage();
            exit(EXIT_SUCCESS);
            break;

        case 'i':
            ping_interval_ms = atoi(optarg);
            if(ping_interval_ms < 100 || ping_interval_ms > 60000)
            {
                ping_interval_ms = default_ping_interval_ms;
                printf("Valid ping interval range is 1000-60000 ms\n");
            }
            break;

        default:
            fprintf(stderr, "Unrecognized argument %c\n", opt);
            break;
        }
    }

    //  Initialize server time samples
    const int num_samples = 10;
	float sct_sample[num_samples];
    float rtt_sample[num_samples];
	int i, sct_sample_index, rtt_sample_index;
	for(i=0; i<num_samples; i++)
	{
		sct_sample[i] = 0.0;
        rtt_sample[i] = 0.0;
	}
	sct_sample_index = 0;
	rtt_sample_index = 0;

	if(argv[optind] == NULL) {
		fprintf(stderr, "missing hostname\n");
        usage();
		exit(EXIT_FAILURE);
	}

	struct addrinfo hints, *servinfo, *p;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

    int retval;
	if ((retval = getaddrinfo(argv[optind], DEFAULT_NP_PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(retval));
		return 1;
	}

	// loop through all the results and connect to the first we can
    int client_sock_fd;
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((client_sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(client_sock_fd, p->ai_addr, p->ai_addrlen) == -1) {
			close(client_sock_fd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	char s[INET6_ADDRSTRLEN];
	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr), s, sizeof s);
	printf("client: connecting to %s\n", s);

	freeaddrinfo(servinfo); // all done with this structure

    //  Configure ping timer
	int timer_fd = timerfd_create(CLOCK_REALTIME, 0);
	if(timer_fd == -1)
	{
		perror("timerfd_create");
		return EXIT_FAILURE;
	}

	struct timespec now_time;
	if(clock_gettime(CLOCK_REALTIME, &now_time) == -1)
	{
		perror("clock_gettime");
		return EXIT_FAILURE;
	}

    printf("Using ping interval %d ms\n", ping_interval_ms);
	struct itimerspec new_time;
	new_time.it_value.tv_sec = now_time.tv_sec + 1;
	new_time.it_value.tv_nsec = now_time.tv_nsec;
	new_time.it_interval = ms2timespec(ping_interval_ms);
	if(timerfd_settime(timer_fd, TFD_TIMER_ABSTIME, &new_time, NULL) == -1)
	{
		perror("timerfd_settime");
		return EXIT_FAILURE;
	}

	fd_set read_fds;
	int max_fd, activity, idx, bytes_received, bytes_read, bytes_sent;
    short seq = 0;
    bool waiting_for_response = false;
    uint32_t total_pings = 0;
    uint32_t lost_pings = 0;
    struct timespec time;
    uint64_t ping_tx_time_ns, ping_rx_time_ns, exp;
	float avg_sct, avg_rtt;
    while(1)
    {
		FD_ZERO(&read_fds);
		FD_SET(client_sock_fd, &read_fds);
		FD_SET(timer_fd, &read_fds);
		max_fd = timer_fd;

		//  Wait for activity
		activity = select(max_fd + 1, &read_fds, NULL, NULL, NULL);
		if((activity < 0) && (errno != EINTR))
		{
			perror("select");
		}

        //  Check timer
        if(FD_ISSET(timer_fd, &read_fds))
        {
            //printf("timer\n");
            if((bytes_read = read(timer_fd, &exp, sizeof(uint64_t))) == 8)
            {
                if(waiting_for_response)
                {
                    lost_pings++;
                }

                clock_gettime(CLOCK_REALTIME, &time);
                ping_tx_time_ns = time.tv_sec * 1e9 + time.tv_nsec;
                //printf("The time in ns is: %lu\n", time_ns);

                np_hdr hdr;
                hdr.type = NP_MSG_PING;
                hdr.seq = seq++;

				idx = write_np_hdr(buffer, buffer_size, &hdr);

				//  Send the message to server
                printf("Pinging server\n");
                bytes_sent = send(client_sock_fd, buffer, idx, 0);
                if(bytes_sent != idx)
                {
                    perror("send");
                }
                
                waiting_for_response = true;
                total_pings++;
            }
        }

		//  Check for IO from server
        if(FD_ISSET(client_sock_fd, &read_fds))
        {
            bytes_received = recv(client_sock_fd, buffer, buffer_size - 1, 0);
            {
                if(bytes_received >= 4)
                {
                    np_hdr hdr;
                    bytes_read = read_np_hdr(buffer, bytes_received, &hdr);
                    if(bytes_read != 4)
                    {
                        printf("Something weird happened\n");
                        continue;
                    }

                    printf("Received message type %d with seq %d\n", hdr.type, hdr.seq);

                    switch(hdr.type) {
                    case NP_MSG_SERVER_TIME:
                        if(bytes_received >= 12)
                        {
                            np_svr_time st;
                            bytes_read = read_np_svr_time(buffer + 4, bytes_received - 4, &st);
                            //printf("Server time in ns is: %lu\n", st.time);

                            clock_gettime(CLOCK_REALTIME, &now_time);
                            uint64_t time_ns = now_time.tv_sec * 1e9 + now_time.tv_nsec;
                            //printf("Client time in ns is: %lu\n", time_ns);

                            //printf("Time delta in ns: %lu\n", time_ns - st.time);

                            sct_sample[sct_sample_index] = (time_ns - st.time) / 1e6f;
                            printf("SCT ms: %f\n", sct_sample[sct_sample_index]);

                            sct_sample_index++;
                            if(sct_sample_index == num_samples)
                            {
                                sct_sample_index = 0;
                            }

                            avg_sct = 0.0;
                            for(i=0; i<num_samples; i++)
                            {
                                avg_sct += sct_sample[i];
                            }
                            avg_sct /= num_samples;
                            printf("Avg SCT ms: %f\n", avg_sct);

                        }
                        break;

                    case NP_MSG_PING_RESPONSE:
                        waiting_for_response = false;

                        clock_gettime(CLOCK_REALTIME, &time);
                        ping_rx_time_ns = time.tv_sec * 1e9 + time.tv_nsec;

                        rtt_sample[rtt_sample_index] = (ping_rx_time_ns - ping_tx_time_ns) / 1e6f;
                        printf("RTT ms: %f\n", rtt_sample[rtt_sample_index]);

                        rtt_sample_index++;
                        if(rtt_sample_index == num_samples)
                        {
                            rtt_sample_index = 0;
                        }

                        avg_rtt = 0.0;
                        for(i=0; i<num_samples; i++)
                        {
                            avg_rtt += rtt_sample[i];
                        }
                        avg_rtt /= num_samples;
                        printf("Avg RTT ms: %f\n", avg_rtt);

                        break;

                    default:
                        printf("Unrecognized message type\n");
                        break;
                    }
                }
            }
		}  //  IO check
	} //  While

	close(client_sock_fd);

	return 0;
}
