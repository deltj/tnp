#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <netdb.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "np_common.h"

#define BACKLOG 10

static volatile bool keep_running = true;

typedef struct s_client_state
{
    int fd;
    unsigned short rx_seq;
    unsigned short tx_seq;
} client_state;

void signal_handler(int signal)
{
    if(signal == SIGINT)
    {
        keep_running = false;
    }
}

int main(int argc, char **argv)
{
    const int max_clients = 30;
    client_state client[max_clients];
    int i;

    struct sockaddr_in address;
    int addrlen;

    const size_t buffer_size = 1000;
    unsigned char buffer[buffer_size];

    uint64_t exp;

    //  Silence unused variable warning for argc,argv.
    //  Annoyingly, removing the name causes an error in gcc 9.4 but the unused
    //  names cause a warning in gcc 12.2
    (void)argc;
    (void)argv;

    signal(SIGINT, signal_handler);

    //  Configure program options
    int send_server_time = false;
    const int default_stime_interval_ms = 5000;
    int stime_interval_ms;
    struct option long_opts[] =
    {
        { "send-server-time",   no_argument,        0, 's' },
        { "interval",           required_argument,  0, 'i' },
        { 0, 0, 0, 0 }
    };

    const char *optstr = "i:s";
    int opt;
    while((opt = getopt_long(argc, argv, optstr, long_opts, &optind)) != -1)
    {
        switch(opt)
        {
        case 'i':
            stime_interval_ms = atoi(optarg);
            if(stime_interval_ms < 1000 || stime_interval_ms > 60000)
            {
                stime_interval_ms = default_stime_interval_ms;
                printf("Valid server time interval range is 1000-60000 ms\n");
            }
            printf("Using server time interval %d ms\n", stime_interval_ms);
            break;

        case 's':
            send_server_time = true;
            printf("Sending server time to clients\n");
            break;
            
        default:
            fprintf(stderr, "Unrecognized argument %c\n", opt);
            break;
        }
    }

    //  Initialize clients
    for(i=0; i<max_clients; i++)
    {
        client[i].fd = 0;
        client[i].rx_seq = 0;
        client[i].tx_seq = 0;
    }

    //  Find an address to bind to
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int retval;
    if((retval = getaddrinfo(NULL, DEFAULT_NP_PORT, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(retval));
        return EXIT_FAILURE;
    }

    int server_sock_fd;
    int yes = 1;
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if((server_sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
        {
            perror("socket");
            continue;
        }

        if(setsockopt(server_sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            perror("setsockopt");
            return EXIT_FAILURE;
        }

        if(bind(server_sock_fd, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(server_sock_fd);
            perror("bind");
            continue;
        }

        break;
    }

    freeaddrinfo(servinfo);

    if(p == NULL)
    {
        fprintf(stderr, "failed to bind\n");
        return EXIT_FAILURE;
    }

    if(listen(server_sock_fd, BACKLOG) == -1)
    {
        perror("listen");
        return EXIT_FAILURE;
    }

    int timer_fd = timerfd_create(CLOCK_REALTIME, 0);
    if(timer_fd == -1)
    {
        perror("timerfd_create");
        return EXIT_FAILURE;
    }

    //  Configure a timer for sending server time to clients
    struct timespec now_time;
    if(clock_gettime(CLOCK_REALTIME, &now_time) == -1)
    {
        perror("clock_gettime");
        return EXIT_FAILURE;
    }

    struct itimerspec new_time;
    new_time.it_value.tv_sec = now_time.tv_sec + 1;
    new_time.it_value.tv_nsec = now_time.tv_nsec;
    new_time.it_interval.tv_sec = 1;
    new_time.it_interval.tv_nsec = 0;
    if(timerfd_settime(timer_fd, TFD_TIMER_ABSTIME, &new_time, NULL) == -1)
    {
        perror("timerfd_settime");
        return EXIT_FAILURE;
    }

    fd_set read_fds;
    int max_fd, fd, activity, idx, bytes_received, bytes_read, bytes_sent;
    uint64_t time_ns;
    int new_fd;
    struct sockaddr_storage their_addr;
    socklen_t sin_size;
    char s[INET6_ADDRSTRLEN];
    np_hdr rx_hdr, tx_hdr;
    while(keep_running)
    {
        FD_ZERO(&read_fds);
        FD_SET(server_sock_fd, &read_fds);
        FD_SET(timer_fd, &read_fds);
        max_fd = max(server_sock_fd, timer_fd);

        for(i=0; i<max_clients; i++)
        {
            fd = client[i].fd;
            if(fd > 0)
            {
                FD_SET(fd, &read_fds);

                if(fd > max_fd)
                {
                    max_fd = fd;
                }
            }
        }

        //  Wait for activity
        activity = select(max_fd + 1, &read_fds, NULL, NULL, NULL);

        //  Weird bug where the program doesn't respond after SIGINT 
        if(!keep_running) break;

        if((activity < 0) && (errno != EINTR))
        {
            perror("select");
            continue;
        }

        //  Check timer
        if(FD_ISSET(timer_fd, &read_fds))
        {
            if((bytes_read = read(timer_fd, &exp, sizeof(uint64_t))) == 8 && send_server_time)
            {
                clock_gettime(CLOCK_REALTIME, &now_time);
                time_ns = now_time.tv_sec * 1e9 + now_time.tv_nsec;
                //printf("The time in ns is: %lu\n", time_ns);

                printf("Sending SERVER_TIME message to clients\n");

                //  Send the message to all clients
                for(i=0; i<max_clients; i++)
                {
                    if(client[i].fd != 0)
                    {
                        tx_hdr.type = NP_MSG_SERVER_TIME;
                        tx_hdr.seq = client[i].tx_seq;
                        np_svr_time tm;
                        tm.time = time_ns;

                        idx = write_np_hdr(buffer, buffer_size, &tx_hdr);
                        idx += write_np_svr_time(buffer + 4, buffer_size - 4, &tm);

                        bytes_sent = send(client[i].fd, buffer, idx, 0);
                        if(bytes_sent != idx)
                        {
                            perror("send");
                        }

                        client[i].tx_seq += 1;
                    }
                }
            }
        }

        //  Check for new connection
        if(FD_ISSET(server_sock_fd, &read_fds))
        {
            sin_size = sizeof their_addr;
            new_fd = accept(server_sock_fd, (struct sockaddr *)&their_addr, &sin_size);
            if(new_fd == -1)
            {
                perror("accept");
                continue;
            }

            inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
            printf("server: got connection from %s\n", s);

            for(i=0; i<max_clients; i++)
            {
                if(client[i].fd == 0)
                {
                    client[i].fd = new_fd;
                    client[i].rx_seq = 0;
                    client[i].tx_seq = 0;
                    break;
                }
            }
        }

        //  Check for IO on existing connections
        for(i=0; i<max_clients; i++)
        {
            fd = client[i].fd;

            if(FD_ISSET(fd, &read_fds))
            {
                if((bytes_received = read(fd, buffer, buffer_size)) == 0)
                {
                    getpeername(fd, (struct sockaddr*)&address, (socklen_t*)&addrlen);
                    printf("Host disconnected, ip %s, port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

                    close(fd);
                    client[i].fd = 0;
                }
                else
                {
                    //printf("received %d bytes from a client\n", bytes_received);

                    if(bytes_received >= 4)
                    {
                        bytes_read = read_np_hdr(buffer, bytes_received, &rx_hdr);
                        if(bytes_read != 4)
                        {
                            printf("Something weird happened\n");
                            continue;
                        }

                        printf("Received message type %d with seq %d\n", rx_hdr.type, rx_hdr.seq);

                        switch(rx_hdr.type) {
                        case NP_MSG_PING:
                            {
                                printf("Received a ping, responding\n");
                                tx_hdr.type = NP_MSG_PING_RESPONSE;
                                tx_hdr.seq = rx_hdr.seq;

                                idx = write_np_hdr(buffer, buffer_size, &tx_hdr);

                                bytes_sent = send(fd, buffer, idx, 0);
                                //printf("wrote %d bytes\n", bytes_sent);
                            }
                            break;

                        default:
                            printf("Unrecognized message type\n");
                            break;
                        }
                    }
                }
            }
        }
    }

    printf("ayy lmao\n");
    return EXIT_SUCCESS;
}
