#ifndef NP_COMMON_H
#define NP_COMMON_H

#include <stdint.h>

#include <arpa/inet.h>

#define DEFAULT_NP_PORT "44404"

#define max(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b;       \
})

typedef enum e_np_msg_type
{
	NP_MSG_SERVER_TIME,
	NP_MSG_CLIENT_TIME,
    NP_MSG_PING,
    NP_MSG_PING_RESPONSE,

	NP_MSG_MAX
} NP_MSG;
	
typedef struct s_np_hdr
{
	short type;
	short seq;
} np_hdr;

typedef struct s_np_svr_time
{
	uint64_t time;
} np_svr_time;

/**
 * Convert a time interval in milliseconds to a timespec, suitable for use with
 * timerfd_create, for example.
 *
 * @param ns A time interval in milliseconds to convert
 * @return A timespec struct
 */
struct timespec ms2timespec(int ms);

void *get_in_addr(struct sockaddr *sa);

int read_np_hdr(unsigned char *buf, int buf_len, np_hdr *hdr);
int write_np_hdr(unsigned char *buf, int buf_len, np_hdr *hdr);

int read_np_svr_time(unsigned char *buf, int buf_len, np_svr_time *st);
int write_np_svr_time(unsigned char *buf, int buf_len, np_svr_time *st);

#endif

