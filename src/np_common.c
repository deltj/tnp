#include <arpa/inet.h>
#include <stddef.h>
#include <string.h>

#include "np_common.h"

struct timespec ms2timespec(int ms)
{
    struct timespec ts;
    ts.tv_sec = ms / 1e3;
	ts.tv_nsec = (ms - ts.tv_sec * 1e3) * 1e6;
    return ts;
}

void *get_in_addr(struct sockaddr *sa)
{
    if(sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int read_np_hdr(unsigned char *buf, int buf_len, np_hdr *hdr)
{
	if(buf == NULL || hdr == NULL || buf_len < 4)
	{
		return 0;
	}

	hdr->type = buf[0] << 8 | buf[1];
	hdr->seq =  buf[2] << 8 | buf[3];

	return 4;
}

int write_np_hdr(unsigned char *buf, int buf_len, np_hdr *hdr)
{
	if(buf == NULL || hdr == NULL || buf_len < 4)
	{
		return 0;
	}

	buf[0] = (hdr->type >> 8);
	buf[1] =  hdr->type;

	buf[2] = (hdr->seq >> 8);
	buf[3] =  hdr->seq;

	return 4;
}

int read_np_svr_time(unsigned char *buf, int buf_len, np_svr_time *st)
{
	if(buf == NULL || st == NULL || buf_len < 8)
	{
		return 0;
	}

	st->time = buf[0];
    st->time <<= 8;
    st->time |= buf[1];
    st->time <<= 8;
    st->time |= buf[2];
    st->time <<= 8;
	st->time |= buf[3];
    st->time <<= 8;
	st->time |= buf[4];
    st->time <<= 8;
	st->time |= buf[5];
    st->time <<= 8;
	st->time |= buf[6];
    st->time <<= 8;
	st->time |= buf[7];

	return 8;
}

int write_np_svr_time(unsigned char *buf, int buf_len, np_svr_time *st)
{
	if(buf == NULL || st == NULL || buf_len < 8)
	{
		return 0;
	}

	buf[0] = st->time >> 56;
	buf[1] = st->time >> 48;
	buf[2] = st->time >> 40;
	buf[3] = st->time >> 32;
	buf[4] = st->time >> 24;
	buf[5] = st->time >> 16;
	buf[6] = st->time >> 8;
	buf[7] = st->time;

	return 8;
}

